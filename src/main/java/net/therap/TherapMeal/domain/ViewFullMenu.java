package net.therap.TherapMeal.domain;

import net.therap.TherapMeal.services.DayItemPair;
import net.therap.TherapMeal.services.TablesInDatabase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author mirajul.mohin
 * @since 2/11/20
 */
public class ViewFullMenu {

    public static void viewItem(List<DayItemPair> menus, Connection con, ResultSet rt) throws SQLException {

        TablesInDatabase td = new TablesInDatabase(con);
        List<String> tableName = td.tableNames(con);

        try {
            while (rt.next()) {

                int id = rt.getInt("day_id");
                String day = rt.getString("day_name");

                String item = "";
                String prefix = "";
                for (String table : tableName) {
                    if (table.equals("meal")) {
                        continue;
                    }
                    prefix = "(" + table + ")";
                    String SQL_SELECT = "Select * from " + table + " where day_id=?";
                    PreparedStatement ps = con.prepareStatement(SQL_SELECT);
                    ps.setInt(1, id);
                    ResultSet rs = ps.executeQuery();

                    item = item + prefix;

                    while (rs.next()) {

                        item = item + rs.getString("item_name") + ",";

                    }
                    item += "||   ";
                }
                menus.add(new DayItemPair(day, item));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        printMenu(menus);
    }

    public static void printMenu(List<DayItemPair> menus) {
        for (DayItemPair menu : menus) {
            System.out.println(menu.getDay() + ": " + menu.getItem());
        }
    }
}
