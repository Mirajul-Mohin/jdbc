package net.therap.TherapMeal.domain;

import net.therap.TherapMeal.services.DayItemPair;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author mirajul.mohin
 * @since 2/11/20
 */
public class ViewIndividualMenu {

    public static void individualMenu(List<DayItemPair> menus, Connection con, ResultSet rt, String db) throws SQLException {
        String item = "";
        System.out.println(db.toUpperCase() + " Menu");
        while (rt.next()) {
            int index = rt.getInt("day_id");
            //String day = rt.getString("day_name");
            String sql = "SELECT * from " + db + " where day_id=?;";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setInt(1, index);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                item = item + resultSet.getString("item_name") + ",";
            }
            System.out.println(rt.getString("day_name") + " : " + item);
            item = "";
        }
    }
}
