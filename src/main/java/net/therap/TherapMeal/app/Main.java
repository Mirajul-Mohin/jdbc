package net.therap.TherapMeal.app;

import net.therap.TherapMeal.domain.ViewFullMenu;
import net.therap.TherapMeal.domain.ViewIndividualMenu;
import net.therap.TherapMeal.services.AddingItem;
import net.therap.TherapMeal.services.DayItemPair;
import net.therap.TherapMeal.services.DeletingItem;
import net.therap.TherapMeal.services.TablesInDatabase;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author mirajul.mohin
 * @since 2/10/20
 */
public class Main {

    public static void main(String[] args) throws SQLException {

        String SQL_SELECT = "Select * from meal";
        List<DayItemPair> menus = new ArrayList<>();

        System.out.println("1. View Complete Menu");
        System.out.println("2. Add Item");
        System.out.println("3. Delete Item");
        System.out.println("4. View Individual Menu");

        Scanner sc = new Scanner(System.in);
        int nextValue = sc.nextInt();

        Connection conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/therap_meal",
                "mirajul.mohin", "therap");
        PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT);

        ResultSet resultSet = preparedStatement.executeQuery();
        if (nextValue == 1) {
            ViewFullMenu.viewItem(menus, conn, resultSet);
        } else if (nextValue == 2) {
            System.out.println("1. Add Breakfast");
            System.out.println("2. Add Lunch");
            System.out.println("3. Add Dinner");

            Scanner scan = new Scanner(System.in);
            int option = scan.nextInt();

            List<String> tableNames = TablesInDatabase.tableNames(conn);

            if (option == 1) {
                AddingItem.addItem(menus, conn, resultSet, "breakfast");
            } else if (option == 2) {
                AddingItem.addItem(menus, conn, resultSet, "lunch");
            } else if (option == 3) {
                AddingItem.addItem(menus, conn, resultSet, "dinner");
            } else {
                System.out.println("Select right menu");
            }

        } else if (nextValue == 3) {
            System.out.println("1. Delete Breakfast");
            System.out.println("2. Delete Lunch");
            System.out.println("3. Delete Dinner");

            Scanner scan = new Scanner(System.in);
            int option = scan.nextInt();

            List<String> tableNames = TablesInDatabase.tableNames(conn);

            if (option == 1) {
                DeletingItem.deleteItem(menus, conn, resultSet, "breakfast");
            } else if (option == 2) {
                DeletingItem.deleteItem(menus, conn, resultSet, "lunch");
            } else if (option == 3) {
                DeletingItem.deleteItem(menus, conn, resultSet, "dinner");
            } else {
                System.out.println("Select right menu");
            }
        } else if (nextValue == 4) {
            System.out.println("1. View Breakfast Menu");
            System.out.println("2. View Lunch Menu");
            System.out.println("3. View Dinner Menu");

            Scanner scan = new Scanner(System.in);
            int option = scan.nextInt();

            List<String> tableNames = TablesInDatabase.tableNames(conn);

            if (option == 1) {
                ViewIndividualMenu.individualMenu(menus, conn, resultSet, "breakfast");
            } else if (option == 2) {
                ViewIndividualMenu.individualMenu(menus, conn, resultSet, "lunch");
            } else if (option == 3) {
                ViewIndividualMenu.individualMenu(menus, conn, resultSet, "dinner");
            } else {
                System.out.println("Select right menu");
            }
        }
    }
}

