package net.therap.TherapMeal.services;

/**
 * @author mirajul.mohin
 * @since 2/11/20
 */
public class DayItemPair {

    private String day;

    private String item;

    public DayItemPair(String day, String item) {
        this.day = day;
        this.item = item;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }
}
