package net.therap.TherapMeal.services;

import net.therap.TherapMeal.domain.ViewFullMenu;
import net.therap.TherapMeal.domain.ViewIndividualMenu;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

/**
 * @author mirajul.mohin
 * @since 2/11/20
 */
public class DeletingItem {

    public static void deleteItem(List<DayItemPair> menus, Connection con, ResultSet rt, String db) throws SQLException {

        ViewIndividualMenu.individualMenu(menus, con, rt, db);

        System.out.print("Enter day: ");
        Scanner scan = new Scanner(System.in);
        String weekDay = scan.nextLine();
        System.out.print("Enter item:");
        String itemName = scan.nextLine();

        weekDay = weekDay.trim().toLowerCase();
        itemName = itemName.trim().toLowerCase();


        String sql = "SELECT * from meal";
        PreparedStatement ps = con.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        int dayId = 0;
        while (rs.next()) {

            String getDay = rs.getString("day_name");
            if (weekDay.equals(getDay)) {
                dayId = rs.getInt("day_id");
            }
        }

        String stmt = "DELETE FROM " + db + " WHERE item_name='" + itemName + "' and day_id=" + dayId;

        PreparedStatement prepstmt = con.prepareStatement(stmt);
        int numberOfUpdate = prepstmt.executeUpdate();

        String SQL_SELECT = "Select * from meal";
        PreparedStatement preparedStatement = con.prepareStatement(SQL_SELECT);

        ResultSet resultSet = preparedStatement.executeQuery();
        ViewFullMenu.viewItem(menus, con, resultSet);
        ViewFullMenu.printMenu(menus);
    }
}
