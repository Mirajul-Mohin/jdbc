package net.therap.TherapMeal.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author mirajul.mohin
 * @since 2/11/20
 */
public class TablesInDatabase {
    Connection con;

    public TablesInDatabase(Connection con) {
        this.con = con;
    }

    public static List<String> tableNames(Connection con) throws SQLException {
        String tables = "SELECT table_name FROM information_schema.tables WHERE table_type = 'base table' AND table_schema='therap_meal'";
        PreparedStatement preparedStatement = con.prepareStatement(tables);

        ResultSet resultSet = preparedStatement.executeQuery();
        List<String> tableName = new ArrayList<>();

        while (resultSet.next()) {
            tableName.add(resultSet.getString("table_name"));
        }
        return tableName;
    }
}
